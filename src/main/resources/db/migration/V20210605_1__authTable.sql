drop table if exists auth;
create table auth(
                        id serial,
                        user_id bigint,
                        login varchar(255),
                        password varchar(255),
                        last_login_timestamp bigint,
                        token varchar(255)
);