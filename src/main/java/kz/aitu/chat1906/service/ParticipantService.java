package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.ChatRepository;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ParticipantService {
    private final ParticipantRepository participantRepository;
    private final UserRepository userRepository;
    private final ChatRepository chatRepository;

    public List<Optional<User>> getUsersByChat(Long chatId) {
        List<Participant> participants = participantRepository.findParticipantsByChatId(chatId);
        List<Optional<User>> users = new ArrayList<>();
        for (Participant participant : participants) {
            users.add(userRepository.findById(participant.getUserId()));
        }
        return users;
    }
    public Boolean isExistByChatAndUser(Long chatId, Long userId){
        List<Participant> participants = participantRepository.findParticipantsByChatId(chatId);
        boolean is = false;
        for (Participant participant : participants) {
            if (participant.getUserId() == userId) {
                is = true;
                break;
            }
        }
        return is;
    }
    public List<Optional<Chat>> getChatsByUser(Long userId) {
        List<Participant> participants = participantRepository.findParticipantByUserId(userId);
        List<Optional<Chat>> chats = new ArrayList<>();
        for (Participant participant : participants) {
            chats.add(chatRepository.findById(participant.getUserId()));
        }
        return chats;
    }

    public void add(Participant participant) {
        participantRepository.save(participant);
    }

    public void delete(Participant participant){
        participantRepository.delete(participant);
    }
}
