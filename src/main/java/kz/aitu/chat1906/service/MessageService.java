package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public List<Message> getMessagesByChat(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    public List<Message> findByChatIdAndUserIdNot(Long chatId, Long userId){
        return messageRepository.findByChatIdAndUserIdNot(chatId,userId);
    }

    public List<Message> findLastMessagesByChatId(Long chatId){return messageRepository.findLastMessagesByChatId(chatId);}

    public List<Message> findLastMessagesByUsertId(Long userId) {return messageRepository.findLastMessagesByUsertId(userId);}

    public List<Message> findNotReadMessages(Long chatid, Long userid) {return messageRepository.findNotReadMessages(chatid,userid);}

    public List<Message> findNotDeliveredMessages(Long chatid, Long userid) {return messageRepository.findNotDeliveredMessages(chatid,userid);}

    public void add(Message message) {
        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        message.setUpdatedTimestamp(date.getTime());
        message.setDelivered(false);
        message.setRead(false);
        messageRepository.save(message);
    }

    public void update(Message message) {
        Date date = new Date();
        message.setUpdatedTimestamp(date.getTime());
        messageRepository.save(message);
    }

    public void delete(Message message) {
        messageRepository.delete(message);
    }
}
