package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant,Long> {
    List<Participant> findParticipantsByChatId(Long chatId);
    List<Participant> findParticipantByUserId(Long userId);
}
