package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepository extends JpaRepository<Auth, Long> {

    Auth getByToken(String token);
    Boolean existsByToken(String token);
}
