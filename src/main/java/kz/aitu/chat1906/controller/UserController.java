package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping("/allAuth")
    public ResponseEntity<?> getAllAuth() {
        return ResponseEntity.ok(authService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> reigster(@RequestBody User user, @RequestBody Auth auth) {
        userService.add(user);
        authService.login(auth);
        return ResponseEntity.ok("User successfully added");
    }

    @PutMapping
    public ResponseEntity<?> login(@RequestBody User user, @RequestBody Auth auth) {
        authService.update(auth);
        userService.update(user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody User user, @RequestBody Auth auth) {
        authService.delete(auth);
        userService.delete(user);
        return ResponseEntity.ok("User successfully deleted");
    }
}
