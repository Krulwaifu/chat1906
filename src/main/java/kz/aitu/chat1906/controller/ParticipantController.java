package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/participant")
public class ParticipantController {
    private final ParticipantService participantService;
    private final AuthService authService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Participant participant,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            participantService.add(participant);
            return ResponseEntity.ok("Participant successfully added");
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Participant participant,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            participantService.delete(participant);
            return ResponseEntity.ok("Participant successfully deleted");
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @GetMapping("/users/{chatId}")
    public ResponseEntity<?> getUsersByChatId(@PathVariable Long chatId,
                                              @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(participantService.getUsersByChat(chatId));
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @GetMapping("/chats/{userId}")
    public ResponseEntity<?> getChatsByUserId(@PathVariable Long userId,
                                              @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(participantService.getChatsByUser(userId));
        }
        return ResponseEntity.ok("Error:Please login");
    }
}
