package kz.aitu.chat1906;

import kz.aitu.chat1906.model.Participant;
import kz.aitu.chat1906.model.User;
import kz.aitu.chat1906.repository.ParticipantRepository;
import kz.aitu.chat1906.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class Chat1906Application {

    public static void main(String[] args) {
        SpringApplication.run(Chat1906Application.class, args);
    }

}
